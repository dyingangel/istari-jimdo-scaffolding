module.exports = function (grunt) {
  require('jit-grunt')(grunt);
  const path = require('path');
  const sass = require('node-sass');
  const timestamp = Date.now();

  grunt.initConfig({
    copy: {
      dev: {
        files: [
          {expand: true, cwd: 'assets/', src: ['**'], dest: 'dist/dev/'},
        ]
      },
      prod: {
        files: [
          {expand: true, cwd: 'assets/', src: ['**'], dest: 'dist/prod/'},
        ]
      }
    },
    assemble: {
      options: {
        layout: 'dev.hbs',
        layoutdir: 'assemble/layout',
        partials: 'assemble/partials/**/*.hbs',
        ext: '.html',
        flatten: true
      },
      dev: {
        files: [
          {
            src: 'assemble/pages/**/*.hbs',
            dest: 'dist/dev',
            expand: true,
            flatten: true,
            ext: '.html',
            extDot: 'last'
          }
        ]
      },
      prod: {
        files: [
          {
            src: 'assemble/pages/**/*.hbs',
            dest: 'dist/prod',
            expand: true,
            flatten: true,
            ext: '.html',
            extDot: 'last'
          }
        ]
      },
    },
    sass: {
      options: {
        implementation: sass,
        style: 'expanded'
      },
      dev: {
        options: {
          sourcemap: 'inline'
        },
        files: {
          'dist/dev/styles.css': 'scss/styles.scss'
        }
      },
      prod: {
        options: {
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: 'scss/',
          src: ['styles.scss'],
          dest: 'dist/prod/',
          rename: function(dest, src){ return dest + src.replace('.scss', '_' + timestamp + '.css') },
        }]
      }
    },
    uglify: {
      dev: {
        options: {
          beautify: true,
          mangle: false,
          compress: false
        },
        files: {
          'dist/dev/scripts.js': ['js/scripts.js']
        }
      },
      prod: {
        files: {
          'dist/prod/scripts.min.js': ['js/scripts.js']
        }
      }
    },
    htmlmin: {
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      prod: {
        files: {
          'dist/prod/index.html': 'dist/prod/index.html'
        }
      }
    },
    postcss: {
      options: {
        processors: [
          require('autoprefixer')(),
          require('cssnano')()
        ]
      },
      dev: {
        options: {
          map: true,
        },
        files: [
          {
            expand: true,
            cwd: 'dist/dev/',
            src: ['*.css'],
            dest: 'dist/dev/',
          }
        ]
      },
      prod: {
        options: {
          map: false,
        },
        files: [
          {
            expand: true,
            cwd: 'dist/prod/',
            src: ['*.css'],
            dest: 'dist/prod/',
          }
        ]
      }
    },
    watch: {
      options: {
        livereload: true
      },
      styles: {
        files: ['scss/**/*.scss'],
        tasks: ['sass:dev', 'postcss:dev']
      },
      html: {
        files: ['assemble/**/*.hbs'],
        tasks: ['assemble:dev']
      },
      js: {
        files: ['js/**/*.js'],
        tasks: ['uglify:dev']
      }
    },
    connect: {
      server: {
        options: {
          base: './dist/dev/',
          port: 4201,
          livereload: 35729
        }
      }
    },
    clean: ['dist']
  });

  grunt.registerTask('serve', ['clean', 'copy:dev', 'assemble:dev', 'sass:dev', 'postcss:dev', 'connect:server', 'watch']);
  grunt.registerTask('release', ['clean', 'copy:prod', 'assemble:prod', 'sass:prod', 'postcss:prod']);
};
